### Seguimos como modelo: Dockerfile - Gancio https://git.sindominio.net/estibadores/gancio/src/branch/master/Dockerfile
### Seguimos como oficial: Dockerfile - Nextcloud https://github.com/nextcloud/docker/blob/master/Dockerfile-debian.template


FROM registry.sindominio.net/debian as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        curl \
        gnupg2 \
        ca-certificates \
        rsync \
        bzip2 \
        busybox-static && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

## entrypoint.sh and cron.sh dependencies (NO SABEMOS SI ES NECESARIO O CÓMO SE HACEN LAS COPIAS DE SEGURIDAD)

## Install the PHP extensions we need https://docs.nextcloud.com/server/stable/admin_manual/installation/source_installation.html

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        libcurl4-openssl-dev \
        libevent-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libldap-common \
        libldap2-dev \
        libmcrypt-dev \
        libmemcached-dev \
        libpng-dev \
        libpq-dev \
        libxml2-dev \
        libmagickwand-dev \
        libzip-dev \
        libwebp-dev \
        libgmp-dev 

## Faltan líneas del original que no sabemos si son necesarios.

## Revisar la creación de la carpeta "data" y los permisos a root
## era `mkdir /var/www/data` Pero el directory `/var/www` no exite y los permisos que tocamos despues
## son del directory `/var/www` y no `/var/www/data`. Entonces es un fallo o hay que crear una carpeta `/var/www/data`?

RUN mkdir /var/www && \
    chown -R www-data:root /var/www && \
    chmod -R g=u /var/www

## Nextcloud download and verification 

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gnupg \
        dirmngr && \
    
    curl -fsSL -o nextcloud.tar.bz2 \
        "https://download.nextcloud.com/server/releases/nextcloud-24.0.1.tar.bz2" && \
    
    curl -fsSL -o nextcloud.tar.bz2.asc \
        "https://download.nextcloud.com/server/releases/nextcloud-24.0.1.tar.bz2.asc" && \
    
    ## (no entendemos si es necesario) export GNUPGHOME="$(mktemp -d)"; \

## gpg key from https://nextcloud.com/nextcloud.asc
    gpg --batch --keyserver keyserver.ubuntu.com --recv-keys 28806A878AE423A28372792ED75899B9A724937A && \
    gpg --batch --verify nextcloud.tar.bz2.asc nextcloud.tar.bz2 && \
    tar -xjf nextcloud.tar.bz2 -C /usr/src/ && \
    gpgconf --kill all && \
    rm nextcloud.tar.bz2.asc nextcloud.tar.bz2 && \
    ## (no entendemos si es necesario) rm -rf "$GNUPGHOME" /usr/src/nextcloud/updater; \
    mkdir -p /usr/src/nextcloud/data && \
    mkdir -p /usr/src/nextcloud/custom_apps && \
    chmod +x /usr/src/nextcloud/occ && \

    ## no deberiamos modificar permisos en data y custom_apps?
    ## no deberiamos modificar propietaria de estas carpetas?
    
    apt-get purge -y --auto-remove -o \
        gnupg \
        dirmngr && \
    rm -rf /var/lib/apt/lists/*

COPY *.sh upgrade.exclude /
COPY config/* /usr/src/nextcloud/config/

ENTRYPOINT ["/entrypoint.sh"]
CMD ["%%CMD%%"]
